import { createStore } from 'redux';

const INITTIAL_STATE = {
    
    surfistaAtivo: {},
    surfistas: [
    {id: 1, nome: 'Victor', diasSurfados: 0},
    {id: 2, nome: 'Enzo', diasSurfados: 0},
    {id: 3, nome: 'Nathan', diasSurfados: 2}]
}
    
        
    



function reducer(state = INITTIAL_STATE, action){
    if(action.type == "Add_Surf_Day"){

        var surfistaSelecionado = state.surfistas.filter((surfista) => surfista.id == action.surfista);
        surfistaSelecionado[0].diasSurfados += 1;
        //console.log(surfistaSelecionado[0]);
        if(action.surfista == 1)
            state.surfistas[0] = surfistaSelecionado[0];
        if(action.surfista == 2)
            state.surfistas[1] = surfistaSelecionado[0];
        if(action.surfista == 3)
            state.surfistas[2] = surfistaSelecionado[0];
     
    return { surfistas: state.surfistas ,surfistaAtivo: surfistaSelecionado[0]}      
    }
    
    return state;
}

const store = createStore(reducer);

export default store;