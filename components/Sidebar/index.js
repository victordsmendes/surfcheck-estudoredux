import React, {Component} from 'react';
import { connect } from 'react-redux';


function AddSurfDay(surfista){
    return{
        type: 'Add_Surf_Day',
        surfista
    }
}


const Sidebar = ({surfistas, dispatch}) => (
    <aside>
    {
        surfistas.map(surfista => (
            <div key={surfista.id} onClick = {() => dispatch(AddSurfDay(surfista.id))}>
                <strong>{surfista.nome}</strong>
            </div>
        ))
    }
    </aside>
);

export default connect(state => ({surfistas: state.surfistas}))(Sidebar);