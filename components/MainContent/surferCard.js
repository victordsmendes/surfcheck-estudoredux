import React, {Component} from 'react';
import {connect} from 'react-redux'

const SurferCard = ({surfista}) => (
    <div>
        <strong>Surfista: {surfista.nome}</strong>
        <span>Dias surfados: {surfista.diasSurfados}</span>
    </div>
)

export default connect(state => ({
   surfista: state.surfistaAtivo  
}))(SurferCard);
