import React, { Component } from 'react';
import {Provider} from 'react-redux'

import Sidebar from './components/Sidebar'
import SurferCard from './components/MainContent/surferCard.js'
import store from './store';

class App extends Component{
  render(){
    return(
      <div className="App" style={{display: 'inline-flex', }}>
        <Provider store={store}>
          <Sidebar/>
          <SurferCard/>
        </Provider>
      </div>
    );
  }
}

export default App;
